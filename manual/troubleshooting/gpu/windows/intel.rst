
****************
Windows -- Intel
****************

.. include:: ../common/introduction.rst
   :start-line: 1

On Windows drivers are provided by the graphics card manufacturer (Intel).
Windows update automatically installs graphics drivers, or
your computer manufacturer may provide its own version of the graphics drivers.

However, these are not always the latest version or may have been corrupted in some way.
We recommend to always use the official drivers.

`Download Latest Intel Drivers <https://www.intel.com/content/www/us/en/support/products/80939/graphics.html>`__

.. include:: ../common/laptops.rst
   :start-line: 1


Compatibility
=============

In some cases Blender may crash on startup. Running Blender in compatibility mode
can help in fixing this issue. To enable compatibility mode, :kbd:`RMB` on
the Blender executable and select :menuselection:`Properties --> Compatibility`
and enable :menuselection:`Run this program in compatibility mode`.
Confirm the changes with *Apply*.

.. include:: ../common/other.rst
   :start-line: 1
