.. index:: Geometry Nodes; Smooth Hair Curves

******************
Smooth Hair Curves
******************

Smoothes the shape of  hair curves.

.. peertube:: 7fpUB2eRT6zjMyHRzJ2ZoJ


Inputs
======

Geometry
   Input Geometry (only curves will be affected).

Amount
   Amount of smoothing. Negative values will result in crumpling the curves.

Shape
   Shape of the influence along curves (0=constant, 0.5=linear).

Iterations
   Amount of smoothing steps.

Weight
   Weight used for smoothing.

Lock Tips
   Lock tip position when smoothing.

Preserve Length
   Preserve each curve's length during deformation.


Properties
==========

This node has no properties.


Outputs
=======

**Geometry**
